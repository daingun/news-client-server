package server;

import java.net.*;
import java.io.*;
import java.util.*;
import static java.util.Comparator.*;

/**
 * Server che consente agli utenti di scambiarsi opinioni sulle notizie.
 */
public final class Server {
    /**
     * Porta su cui il server è in ascolto.
     */
    private static final int SERVER_PORT = 3000;
    /**
     * Dizionario degli utenti registrati (username - password).
     */
    private final Map<String, String> utenti = new HashMap<>();
    /**
     * Lista di notizie contenute nel server.
     */
    private final List<Notizia> notizie = new ArrayList<>();

    /**
     * Costruttore del server. Avvia il ciclo principale del server.
     */
    public Server() {
        System.out.println("[SERVER] Server avviato");
        mainLoop();
    }

    /**
     * Ciclo principale di esecuzione del server. Accetta le connessioni dai client
     * e crea un thread per connessione.
     */
    private void mainLoop() {
        try (ServerSocket serverSocket = new ServerSocket(SERVER_PORT);) {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                ServerThread serverThread = new ServerThread(clientSocket, utenti, notizie);
                Thread thread = new Thread(serverThread);
                thread.start();
            }
        } catch (IOException e) {
            System.out.println("[SERVER] Errore: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new Server();
    }
}

/**
 * Classe che implementa un thread per la connessione con il client.
 */
class ServerThread implements Runnable {
    /**
     * Reader di ricezione dei messaggi dal client.
     */
    private final BufferedReader inSocket;
    /**
     * Writer per inviare i messaggi al client.
     */
    private final PrintWriter outSocket;
    /**
     * Socket di collegamento con il client. Una per thread.
     */
    private final Socket socket;
    /**
     * Dizionario (username - password) di utenti condivisa dai thread.
     */
    private final Map<String, String> utenti;
    /**
     * Lista di notizie condivisa dai thread.
     */
    private List<Notizia> notizie;

    /**
     * Crea un thread di lavoro del server.
     * 
     * @param clientSocket - socket aperta con il client.
     * @param utenti       - Dizionario che contiene le credenziali degli utenti.
     * @param notizie      - Lista contenente tutte le notizie.
     * @throws IOException - Se la comunicazione con il client fallisce.
     */
    ServerThread(Socket clientSocket, Map<String, String> utenti, List<Notizia> notizie)
            throws IOException {
        this.socket = clientSocket;
        this.utenti = utenti;
        this.notizie = notizie;
        try {
            this.inSocket = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.outSocket = new PrintWriter(
                    new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream())),
                    true);
        } catch (IOException e) {
            pulisciRisorse();
            throw e;
        }
    }

    @Override
    public void run() {
        log("Server thread started");
        try {
            threadLoop();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            pulisciRisorse();
        }
    }

    /**
     * Pulisci le risorse occupate dal thread: input buffer, output buffer e socket.
     */
    private void pulisciRisorse() {
        if (this.outSocket != null) {
            this.outSocket.close();
        }
        try {
            if (this.inSocket != null) {
                this.inSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ciclo principale di esecuzione del thread del server.
     * 
     * @throws IOException - Se la comunicazione con il client fallisce.
     */
    private void threadLoop() throws IOException {
        while (true) {
            String messaggio = inSocket.readLine();
            if (messaggio == null) {
                break;
            }
            switch (messaggio) {
            case "login":
                login();
                log("Login terminato");
                break;
            case "aggiungi":
                aggiuntaNotizia();
                log("Aggiunta notizia terminata");
                break;
            case "discuti":
                discussione();
                log("Terminata discussione");
                break;
            case "migliori":
                inviaMiglioriNotizie();
                log("Invio migliori notizie terminata");
                break;
            case "fine":
                log("Disconnessione client");
                return;
            default:
                break;
            }
        }
    }

    /**
     * Effettua le operazioni di login.
     * 
     * @throws IOException - Se la comunicazione con il client fallisce.
     */
    private void login() throws IOException {
        String username = inSocket.readLine();
        String password = inSocket.readLine();
        String loggato = controllaCredenziali(username, password);
        outSocket.println(loggato);
        outSocket.flush();
    }

    /**
     * Verifica che l'utente sia registrato. L'operazione ha successo sia se
     * l'utente non è ancora registrato (viene automaticamente registrato), sia se
     * utente e password sono corretti.
     * 
     * @param username - Username.
     * @param password - Password.
     * @return - "true" se l'autenticazione ha successo altrimenti "false".
     */
    private String controllaCredenziali(String username, String password) {
        synchronized (utenti) {
            String psw = utenti.putIfAbsent(username, password);
            if (psw == null || psw.equals(password)) {
                return "true";
            } else {
                return "false";
            }
        }
    }

    /**
     * Aggiunge una notizia all'elenco.
     * 
     * @throws IOException - Se la comunicazione con il client fallisce.
     */
    private void aggiuntaNotizia() throws IOException {
        String autore = inSocket.readLine();
        String titolo = inSocket.readLine();
        Notizia notizia = new Notizia(autore, titolo);
        salvaNotizia(notizia);
    }

    /**
     * Salva una notizia nell'elenco.
     * 
     * @param notizia - Notizia da salvare.
     */
    private void salvaNotizia(Notizia notizia) {
        synchronized (notizie) {
            notizie.add(notizia);
        }
    }

    /**
     * Invia al client le 10 migliori notizie in base alla media dei voti.
     */
    private void inviaMiglioriNotizie() {
        synchronized (notizie) {
            notizie.stream().sorted(comparingDouble(Notizia::getMediaVoti).reversed()).limit(10)
                    .forEach(outSocket::println);
            // Se le notizie disponibili sono meno di 10 è necessario segnalarlo
            // esplicitamente al client.
            if (notizie.size() < 10) {
                outSocket.println("FINE");
            }
        }
    }

    /**
     * Discuti una notizia.
     * 
     * @throws IOException - Se la comunicazione con il client fallisce.
     */
    private void discussione() throws IOException {
        inviaTutteNotize();
        try {
            // Le nozizie sono aggiunte in coda quindi l'indice di una notizia nella lista
            // rimane costante.
            int indiceNotizia = Integer.parseInt(inSocket.readLine());
            Optional<Notizia> notizia = getNotizia(indiceNotizia);
            if (notizia.isPresent()) {
                inviaCommenti(notizia.get());
                inserisciCommento(notizia.get());
            }
        } catch (NumberFormatException e) {
            log(e.getMessage());
        }
    }

    /**
     * Invia il numero di notizie disponibili e le notizie in forma di stringa al
     * client, preposte dal loro indice nella lista.
     */
    private void inviaTutteNotize() {
        synchronized (notizie) {
            // È necessario inviare al client il numero di notize che verrano spedite.
            outSocket.println(notizie.size());
            for (ListIterator<Notizia> it = notizie.listIterator(); it.hasNext();) {
                // inviare il numero d'indice della notizia e la notizia stessa.
                // L'utilizzo di ListIterator è l'unico modo per avere automaticamente l'indice
                // della notizia.
                outSocket.println(it.nextIndex() + " - " + it.next());
            }
        }
    }

    /**
     * Ottieni la notizia al dato indice.
     * 
     * @param indice - Indice della notizia.
     * @return Notizia.
     */
    private Optional<Notizia> getNotizia(int indice) {
        synchronized (notizie) {
            try {
                return Optional.of(notizie.get(indice));
            } catch (IndexOutOfBoundsException e) {
                return Optional.empty();
            }
        }
    }

    /**
     * Invia i commenti della notizia al client.
     * 
     * @param notizia - Notizia che contiene i commenti da inviare.
     */
    private void inviaCommenti(Notizia notizia) {
        synchronized (notizia) {
            List<Commento> commenti = notizia.getCommenti();
            // È necessario inviare al client il numero di commenti che verrano inoltrati.
            outSocket.println(commenti.size());
            for (Commento commento : commenti) {
                outSocket.println(commento.getVoto());
                outSocket.println(commento.getTesto());
            }
        }
    }

    /**
     * Recupera dal client il commento e inseriscilo nella notizia.
     * 
     * @param notizia - Notizia in cui inserire il commento.
     * @throws IOException - Se la comunicazione con il client fallisce.
     */
    private void inserisciCommento(Notizia notizia) throws IOException {
        String risposta = inSocket.readLine();
        if (!risposta.equals("si")) {
            return;
        }
        try {
            double voto = Double.parseDouble(inSocket.readLine());
            String testo = inSocket.readLine();
            Commento commento = new Commento(testo, voto);
            notizia.aggiungiCommento(commento);
        } catch (NumberFormatException e) {
            log(e.getMessage());
        }
    }

    /**
     * Stampa a terminale il messaggio dato.
     * 
     * @param messaggio - Messaggio da stampare.
     */
    private void log(String messaggio) {
        System.out.println("[SERVER " + Thread.currentThread() + "] " + messaggio);
    }
}

/**
 * Classe che definisce le notizie contenute nel server.
 */
class Notizia {
    /**
     * Autore della notizie.
     */
    private final String autore;
    /**
     * Titolo della notizia.
     */
    private final String titolo;
    /**
     * Commenti collegati alla notizia.
     */
    private final List<Commento> commenti = new ArrayList<>();
    /**
     * Media dei voti dei commenti collegati alla notizia.
     */
    private double mediaVoti = 0.0;

    /**
     * Costruisce una notizia dati autore e titolo.
     * 
     * @param autore - Autore della notizia.
     * @param titolo - Titolo della notizia.
     */
    Notizia(String autore, String titolo) {
        this.autore = autore;
        this.titolo = titolo;
    }

    public String getTitolo() {
        return titolo;
    }

    public String getAutore() {
        return autore;
    }

    /**
     * Aggiungi un commento alla notizia e aggiorna la media dei voti della notizia.
     * 
     * @param commento - Commento da aggiungere.
     */
    public synchronized void aggiungiCommento(Commento commento) {
        if (this.commenti.add(commento)) {
            ricalcolaMediaVoti(commento.getVoto());
        }
    }

    /**
     * Ricalcola la media dei voti in base a quelli presenti nei commenti.
     * 
     * @param nuovoVoto - voto dell'ultimo commento inserito.
     */
    private void ricalcolaMediaVoti(double nuovoVoto) {
        double numeroVoti = this.commenti.size();
        this.mediaVoti = (getMediaVoti() * (numeroVoti - 1.0) + nuovoVoto) / numeroVoti;
    }

    public List<Commento> getCommenti() {
        return this.commenti;
    }

    public double getMediaVoti() {
        return this.mediaVoti;
    }

    @Override
    public String toString() {
        return "Titolo: " + getTitolo() + " | Autore: " + getAutore() + " | Media voti: "
                + String.format("%.2f", getMediaVoti());
    }
}

/**
 * Classe che implementa i commenti collegati alle notizie.
 */
class Commento {
    /**
     * Testo del commento.
     */
    private final String testo;
    /**
     * Voto collegato al commento.
     */
    private final double voto;

    /**
     * Costruisci un Commento dati testo e voto.
     * 
     * @param testo - Testo del commento.
     * @param voto  - Voto collegato al commento.
     */
    Commento(String testo, double voto) {
        this.testo = testo;
        this.voto = voto;
    }

    public String getTesto() {
        return testo;
    }

    public double getVoto() {
        return voto;
    }
}

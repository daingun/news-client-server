package client;

import java.io.IOException;

import client.controller.LoginController;
import client.model.ClientModel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class representing the Client side of the application.
 */
public class Client extends Application {
    /**
     * Model that is available through the life span of the application.
     */
    private ClientModel clientModel;
    /**
     * Width of the scene.
     */
    private static double WIDTH = 800.;
    /**
     * Height of the scene.
     */
    private static double HEIGHT = 600.;

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/client/view/Login.fxml"));
        Parent root = loader.load();

        LoginController controller = loader.getController();
        clientModel = new ClientModel();
        controller.setModel(clientModel);

        primaryStage.setTitle("Client");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
    }

    @Override
    public void stop() {
        clientModel.quit();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
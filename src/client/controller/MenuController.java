package client.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

/**
 * Controller for the Menu view.
 */
public class MenuController extends Controller {
    @FXML
    private GridPane menuPane;
    @FXML
    private Button addNewsButton;
    @FXML
    private Button commentNewsButton;
    @FXML
    private Button seeBestButton;
    @FXML
    private Button quitButton;

    @FXML
    @Override
    void initialize() {
        quitButton.setOnAction(this::quitButtonAction);
        seeBestButton.setOnAction(this::seeBestNewsButtonAction);
        commentNewsButton.setOnAction(this::seeCommentNewsButtonAction);
        addNewsButton.setOnAction(this::addNewsButtonAction);
    }

    /**
     * Bring up the view to add a news.
     * 
     * @param event - Event.
     */
    private void addNewsButtonAction(ActionEvent event) {
        setView("/client/view/AddNews.fxml", menuPane);
    }

    /**
     * Show the list of all news to comment.
     * 
     * @param event - Event.
     */
    private void seeCommentNewsButtonAction(ActionEvent event) {
        AllNewsController controller = setView("/client/view/AllNews.fxml", menuPane);
        controller.fillAllNewsList();
    }

    /**
     * Show the list of the best news.
     * 
     * @param event - Event.
     */
    private void seeBestNewsButtonAction(ActionEvent event) {
        BestNewsController controller = setView("/client/view/BestNews.fxml", menuPane);
        controller.fillBestNewsList();
    }

    /**
     * Quit the application.
     * 
     * @param event - Event.
     */
    private void quitButtonAction(ActionEvent event) {
        Platform.exit();
    }
}

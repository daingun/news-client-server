package client.controller;

import client.model.Comment;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.control.TextField;

/**
 * Controller for the Comment view.
 */
public class CommentController extends Controller {
    @FXML
    private VBox commentPane;
    @FXML
    private ListView<Comment> commentList;
    @FXML
    private TextField vote;
    @FXML
    private TextField comment;
    @FXML
    private Button commentButton;
    @FXML
    private Button backToMenu;

    @FXML
    @Override
    void initialize() {
        commentButton.setOnAction(this::commentButtonAction);
        backToMenu.setOnAction(this::backToMenuAction);
    }

    /**
     * Send the vote and the comment to the server.
     * 
     * @param event - Event.
     */
    private void commentButtonAction(ActionEvent event) {
        String voto = vote.getText();
        try {
            Double.parseDouble(voto);
            clientModel.addComment(voto, comment.getText());
            clearForm();
            setView("/client/view/Menu.fxml", commentPane);
        } catch (NumberFormatException e) {
            vote.setText("Sono ammessi solo numeri");
        }
    }

    /**
     * Go back to the menu without sending the comment.
     * 
     * @param event - Event
     */
    private void backToMenuAction(ActionEvent event) {
        clientModel.rejectComment();
        clearForm();
        setView("/client/view/Menu.fxml", commentPane);
    }

    /**
     * Clear the vote and comment text fields.
     */
    private void clearForm() {
        vote.clear();
        comment.clear();
    }

    /**
     * Fill the list of comments from the server.
     * 
     * @param index index of the selected news.
     */
    void fillCommentList(int index) {
        commentList.setItems(clientModel.getComments(index));
    }
}

package client.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * Controller for the AllNews view.
 */
public class AllNewsController extends Controller {
    @FXML
    private VBox allNewsPane;
    @FXML
    private ListView<String> allNewsList = new ListView<>();

    @FXML
    @Override
    void initialize() {
        allNewsList.setOnMouseClicked(this::onSelectedNewsAction);
    }

    /**
     * Fill the list of news with the ones available on the server.
     */
    void fillAllNewsList() {
        allNewsList.setItems(clientModel.getAllNewsList());
    }

    /**
     * Action performed on the selection of one item of the list. Brings up the view
     * to comment the selected news.
     * 
     * @param event - Event
     */
    private void onSelectedNewsAction(MouseEvent event) {
        CommentController controller = setView("/client/view/Comment.fxml", allNewsPane);
        int index = allNewsList.getSelectionModel().getSelectedIndex();
        controller.fillCommentList(index);
    }
}

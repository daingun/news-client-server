package client.controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * Controller of the Login view.
 */
public class LoginController extends Controller {
    @FXML
    private Pane loginPane;
    @FXML
    private Text loginResult;
    @FXML
    private TextField userName;
    @FXML
    private PasswordField password;
    @FXML
    private Button loginButton;

    @FXML
    @Override
    void initialize() {
        loginButton.setOnAction(this::loginButtonAction);
    }

    /**
     * Handle the login to the server.
     * 
     * @param event - Event.
     */
    private void loginButtonAction(ActionEvent event) {
        String user = userName.getText();
        String pwd = password.getText();
        if (user.isEmpty() || pwd.isEmpty()) {
            loginResult.setText("Il nome utente e la password non possono essere vuoti");
            return;
        }
        try {
            if (clientModel.login(user, pwd)) {
                setView("/client/view/Menu.fxml", loginPane);
            } else {
                loginResult.setText("Nome utente in uso con altra password");
            }
        } catch (IOException e) {
            e.printStackTrace();
            loginResult.setText("Login failed");
            return;
        }
    }
}

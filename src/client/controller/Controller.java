package client.controller;

import java.io.IOException;

import client.model.ClientModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;

/**
 * Abstract class for controllers.
 */
abstract class Controller {
    /**
     * Private reference to the application model.
     */
    protected ClientModel clientModel;

    /**
     * Stores the client model reference into the controller.
     * 
     * @param cm - Client model
     */
    public void setModel(ClientModel cm) {
        clientModel = cm;
    }

    /**
     * Initialize the actions on the UI elements.
     */
    abstract void initialize();

    /**
     * Sets the new view, and the controller, passing the client model.
     * 
     * @param <T>         - returned Controller type.
     * @param view        - new view path.
     * @param currentPane - top level Pane of the view associated to this
     *                    controller.
     * @return The controller associated to the new view.
     */
    protected <T extends Controller> T setView(String view, Node currentPane) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(view));
        try {
            Parent root = loader.load();
            currentPane.getScene().setRoot(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
        T controller = loader.getController();
        controller.setModel(clientModel);
        return controller;
    }
}

package client.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 * Controller for the BestNews view.
 */
public class BestNewsController extends Controller {
    @FXML
    private VBox bestNewsPane;
    @FXML
    private ListView<String> bestNewsList = new ListView<>();
    @FXML
    private Button backToMenu;

    @FXML
    @Override
    void initialize() {
        backToMenu.setOnAction(this::backToMenuAction);
    }

    /**
     * Fill the list with the news sent from the server.
     */
    void fillBestNewsList() {
        bestNewsList.setItems(clientModel.getBestNewsList());
    }

    /**
     * Action performed to go back to the menu.
     * 
     * @param event - Event
     */
    private void backToMenuAction(ActionEvent event) {
        setView("/client/view/Menu.fxml", bestNewsPane);
    }
}

package client.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * Controller for the AddNews view.
 */
public class AddNewsController extends Controller {
    @FXML
    private GridPane insertNewsPane;
    @FXML
    private TextField author;
    @FXML
    private TextField title;
    @FXML
    private Button newNewsButton;

    @FXML
    @Override
    void initialize() {
        newNewsButton.setOnAction(this::newNewsButtonAction);
    }

    /**
     * Send the author and the title of the news to the server.
     * 
     * @param event - Event.
     */
    private void newNewsButtonAction(ActionEvent event) {
        clientModel.addNews(author.getText(), title.getText());
        author.clear();
        title.clear();
        setView("/client/view/Menu.fxml", insertNewsPane);
    }
}

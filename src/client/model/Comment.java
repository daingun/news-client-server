package client.model;

public final class Comment {
    final String vote;
    final String comment;

    Comment(String v, String c) {
        vote = v;
        comment = c;
    }

    @Override
    public String toString() {
        return "Voto:" + vote + " | Commento: " + comment;
    }
}
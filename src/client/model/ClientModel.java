package client.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Client model of the application.
 */
public final class ClientModel {
    /**
     * Server port.
     */
    private final static int PORT = 3000;
    /**
     * Server address.
     */
    private final static String ADDRESS = "localhost";

    /**
     * Communication socket.
     */
    private Socket socket;
    /**
     * Reader to receive messages from the server.
     */
    private BufferedReader inSocket;
    /**
     * Writer to send messages to the server.
     */
    private PrintWriter outSocket;
    /**
     * Writer to the console.
     */
    private PrintWriter outVideo;

    /**
     * Creates the client model.
     */
    public ClientModel() {
        try {
            System.out.println("Il client tenta di connettersi");
            socket = new Socket(ADDRESS, PORT);

            inSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outSocket = new PrintWriter(
                    new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            outVideo = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)),
                    true);
            outVideo.println("Client connesso");
        } catch (IOException e) {
            outVideo.println("Exception: " + e);
            e.printStackTrace();
            try {
                socket.close();
            } catch (IOException ex) {
                outVideo.println("Socket not closed");
            }
        }
    }

    /**
     * Send the credentials to the server.
     * 
     * @param username - User name.
     * @param password - Password.
     * @return true if the credential are valid otherwise false.
     * @throws IOException - If connection communication fails.
     */
    public boolean login(String username, String password) throws IOException {
        outSocket.println("login");
        outSocket.flush();
        outSocket.println(username);
        outSocket.flush();
        outSocket.println(password);
        outSocket.flush();

        boolean loggato = Boolean.valueOf(inSocket.readLine()).booleanValue();
        if (loggato)
            outVideo.println("Login effettuato correttamente");
        else
            outVideo.println("Nome utente in uso con altra password");
        return loggato;
    }

    /**
     * Send to the server the message to close the connection.
     */
    public void quit() {
        outSocket.println("fine");
        outVideo.println("Connection closed.");
        cleanResources();
    }

    /**
     * Clean all the resources.
     */
    private void cleanResources() {
        try {
            if (socket != null) {
                socket.close();
            }
            if (inSocket != null) {
                inSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (outSocket != null) {
            outSocket.close();
        }
        if (outVideo != null) {
            outVideo.close();
        }
    }

    /**
     * Send the news to the server.
     * 
     * @param author - News author.
     * @param title  - News title.
     */
    public void addNews(String author, String title) {
        try {
            outVideo.println("Inizio inserimento dati di una notizia.");
            outSocket.println("aggiungi");
            outSocket.println(author);
            outSocket.println(title);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
    }

    /**
     * Get the best news from the server.
     * 
     * @return A list of news.
     */
    public ObservableList<String> getBestNewsList() {
        List<String> list = new ArrayList<>();
        try {
            outVideo.println("Migliori");
            outSocket.println("migliori");

            for (int i = 0; i < 10; i++) {
                String news = inSocket.readLine();
                if (news.equals("FINE")) {
                    break;
                }
                list.add(news);
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        return FXCollections.observableList(list);
    }

    /**
     * Get all the news from the server.
     * 
     * @return A list of news.
     */
    public ObservableList<String> getAllNewsList() {
        List<String> list = new ArrayList<String>();
        try {
            outVideo.println("Commenti");
            outSocket.println("discuti");
            outVideo.println("Lista notizie in discussione:");
            int numNotizie = Integer.parseInt(inSocket.readLine());

            for (int i = 0; i < numNotizie; i++) {
                String news = inSocket.readLine();
                list.add(news);
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        return FXCollections.observableList(list);
    }

    /**
     * Get all comments for the given news index.
     * 
     * @param index - News index.
     * @return A list of comments.
     */
    public ObservableList<Comment> getComments(int index) {
        List<Comment> list = new ArrayList<>();
        outVideo.println("Selezione notizia");
        outSocket.println(index);

        try {
            int numCommenti = Integer.parseInt(inSocket.readLine());
            for (int i = 0; i < numCommenti; i++) {
                String voto = inSocket.readLine();
                String commento = inSocket.readLine();
                list.add(new Comment(voto, commento));
            }
        } catch (IOException | NumberFormatException e) {
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        return FXCollections.observableList(list);
    }

    /**
     * Send the comment to the server
     * 
     * @param vote    - Comment vote.
     * @param comment - Comment text.
     */
    public void addComment(String vote, String comment) {
        outVideo.println("Aggiunta commento");
        outSocket.println("si");
        outSocket.println(vote);
        outSocket.println(comment);
    }

    /**
     * Send to the server the message that no comment is inserted.
     */
    public void rejectComment() {
        outVideo.println("Rifiuta commento");
        outSocket.println("no");
    }
}

package server;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class NotiziaTest {

    /**
     * Testa che la media dei voti sia correttamente calcolata.
     */
    @Test
    void mediaVotiTest() {
        Notizia n = new Notizia("autore", "titolo");
        Commento c1 = new Commento("primo commento", 5.0);
        n.aggiungiCommento(c1);
        Commento c2 = new Commento("Secondo commento", 10.0);
        n.aggiungiCommento(c2);

        double media = n.getMediaVoti();
        assertEquals(7.5, media);
    }
}

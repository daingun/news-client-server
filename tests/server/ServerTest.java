package server;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ServerTest {

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        Thread mainServerThread = new Thread(() -> new Server());
        mainServerThread.start();
    }

    /**
     * Testa che la connessione avvenga con successo. Cioè che non lanci eccezzioni.
     */
    @Test
    void testConnection() {
        try (ClientDriver cd = new ClientDriver();) {
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Testa il login corretto di un client.
     */
    @Test
    void testSuccessfullLogin() {
        try (ClientDriver cd = new ClientDriver();) {
            boolean loggato = cd.login("user1", "pwd1");
            assertTrue(loggato);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Testa il login fallito di un client, dato dal fatto che il nome utente è già
     * presente con un'altra password.
     */
    @Test
    void testFailedLogin() {
        try (ClientDriver cd1 = new ClientDriver(); ClientDriver cd2 = new ClientDriver();) {
            boolean loggato1 = cd1.login("user2", "pwd2");
            assertTrue(loggato1);

            boolean loggato2 = cd2.login("user2", "pwdWrong");
            assertFalse(loggato2);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Testa l'aggiunta di una notizia e la presenza della stessa nel server.
     */
    @Test
    void testAddNews() {
        try (ClientDriver cd = new ClientDriver();) {
            cd.login("user1", "pwd1");
            cd.aggiungiNotizia("autore0", "prima notizia");
            String autore = "autore da trovare";
            String titolo = "notizia da trovare";
            cd.aggiungiNotizia(autore, titolo);

            List<String> notizie = cd.ottieniTutteLeNotizieSenzaCommentare();
            String expected = String.format("Titolo: %s | Autore: %s | Media voti: 0,00", titolo,
                    autore);
            assertTrue(notizie.stream().anyMatch(n -> n.contains(expected)));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Testa il ricevimento delle 10 migliori notizie. Vengono create 12 notizie.
     */
    @Test
    void testBestNews() {
        try (ClientDriver cd = new ClientDriver();) {
            cd.login("user3", "pwd3");
            for (int i = 0; i < 12; i++) {
                cd.aggiungiNotizia("autore" + i, "titolo " + i);
            }
            List<String> notizie = cd.ottieniLeNotizieMigliori();
            assertTrue(!notizie.isEmpty());
            assertEquals(10, notizie.size());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Testa l'inserimento di un commento e del relativo voto.
     */
    @Test
    void testCommentNews() {
        try (ClientDriver cd = new ClientDriver();) {
            cd.login("user4", "pwd4");
            cd.aggiungiNotizia("autore 4", "titolo 4");
            String commento = "primo commento";
            cd.commentaNotizia(0, 10.0, commento);

            List<String> commenti2 = cd.commentiDellaPrimaNotizia();
            assertTrue(!commenti2.isEmpty());
            assertEquals(1, commenti2.size());
            assertEquals(String.format("10.0;%s", commento), commenti2.get(0));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Testa che la notizia con il voto più alto sia al primo posto nella lista
     * delle migliori notizie.
     */
    @Test
    void testHighVoteNews() {
        try (ClientDriver cd = new ClientDriver();) {
            cd.login("user5", "pwd5");
            cd.aggiungiNotizia("autore 5", "titolo 5");
            cd.aggiungiNotizia("autore 6", "titolo 6");
            String commento = "migliore commento";
            cd.commentaNotizia(1, 100.0, commento);
            List<String> notizie = cd.ottieniLeNotizieMigliori();
            String[] mediaVoti = notizie.get(0).split(" \\| ");
            assertEquals("Media voti: 100,00", mediaVoti[2]);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}

/**
 * Classe per testare il server.
 */
class ClientDriver implements AutoCloseable {
    private final static int PORT = 3000;
    private final static String ADDRESS = "localhost";

    private Socket socket;
    private BufferedReader inSocket;
    private PrintWriter outSocket;

    ClientDriver() {
        try {
            socket = new Socket(ADDRESS, PORT);
            inSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outSocket = new PrintWriter(
                    new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            e.printStackTrace();
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                System.err.println("Socket not closed");
            }
        }
    }

    /**
     * Effettua il login al server.
     * 
     * @param user - Nome utente.
     * @param pwd  - Password.
     * @return Vero se il login ha avuto successo.
     * @throws IOException - Se ci sono errori di connessione.
     */
    boolean login(String user, String pwd) throws IOException {
        outSocket.println("login");
        outSocket.println(user);
        outSocket.println(pwd);
        return Boolean.valueOf(inSocket.readLine()).booleanValue();
    }

    /**
     * Aggiungi una notizia sul server.
     * 
     * @param autore - Autore della notizia.
     * @param titolo - Titolo della notizia.
     */
    void aggiungiNotizia(String autore, String titolo) {
        outSocket.println("aggiungi");
        outSocket.println(autore);
        outSocket.println(titolo);
    }

    /**
     * Ottieni tutte le notizie presenti sul server senza inserire alcun commento.
     * 
     * @return La lista di tutte le notizie.
     * @throws NumberFormatException
     * @throws IOException
     */
    List<String> ottieniTutteLeNotizieSenzaCommentare() throws NumberFormatException, IOException {
        outSocket.println("discuti");
        int numLibri = Integer.parseInt(inSocket.readLine());
        List<String> list = new ArrayList<>();
        for (int i = 0; i < numLibri; i++) {
            String news = inSocket.readLine();
            list.add(news);
        }
        outSocket.println("-1");
        return list;
    }

    /**
     * Ottieni le migliri notizie presenti sul server.
     * 
     * @return La lista delle migliori notizie.
     * @throws IOException - Se la connessione da errori.
     */
    List<String> ottieniLeNotizieMigliori() throws IOException {
        outSocket.println("migliori");
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String news = inSocket.readLine();
            if (news.equals("FINE")) {
                break;
            }
            list.add(news);
        }
        return list;
    }

    /**
     * Commenta una notizia.
     * 
     * @param indice   - Indice della notizia.
     * @param voto     - Voto dato alla notizia.
     * @param commento - Testo del commento.
     * @throws NumberFormatException
     * @throws IOException
     */
    void commentaNotizia(int indice, double voto, String commento)
            throws NumberFormatException, IOException {
        outSocket.println("discuti");
        int numLibri = Integer.parseInt(inSocket.readLine());
        List<String> listaNotizie = new ArrayList<>();
        for (int i = 0; i < numLibri; i++) {
            String news = inSocket.readLine();
            listaNotizie.add(news);
        }
        outSocket.println(indice);

        int numCommenti = Integer.parseInt(inSocket.readLine());
        List<String> listaCommenti = new ArrayList<>();
        for (int i = 0; i < numCommenti; i++) {
            String v = inSocket.readLine();
            String c = inSocket.readLine();
            listaCommenti.add(v + ";" + c);
        }
        outSocket.println("si");
        outSocket.println(voto);
        outSocket.println(commento);
    }

    /**
     * Ottieni i commenti associati alla prima notizia presente sul server.
     * 
     * @return La lista dei commenti della notizia.
     * @throws NumberFormatException
     * @throws IOException
     */
    List<String> commentiDellaPrimaNotizia() throws NumberFormatException, IOException {
        outSocket.println("discuti");
        int numLibri = Integer.parseInt(inSocket.readLine());
        List<String> listaNotizie = new ArrayList<>();
        for (int i = 0; i < numLibri; i++) {
            String news = inSocket.readLine();
            listaNotizie.add(news);
        }
        outSocket.println("0");

        int numCommenti = Integer.parseInt(inSocket.readLine());
        List<String> listaCommenti = new ArrayList<>();
        for (int i = 0; i < numCommenti; i++) {
            String v = inSocket.readLine();
            String c = inSocket.readLine();
            listaCommenti.add(v + ";" + c);
        }
        outSocket.println("no");
        return listaCommenti;
    }

    /**
     * Disconnetti il client dal server.
     */
    @Override
    public void close() throws Exception {
        outSocket.println("fine");
        socket.close();
    }
}

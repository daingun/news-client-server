# Java client/server news aggregator

Client-server software that allows the users to exchange opinions on news found on the internet.

## Client side

Graphical User Interface that connects with the server, built with JavaFX.

## Server side

Stores news and comments, handles one connection per thread.
